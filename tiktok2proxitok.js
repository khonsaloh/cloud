// ==UserScript==
// @name         Tiktok to Proxitok
// @namespace    gonzalo 
// @version      1.0
// @description  redirige enlaces de tiktok a instancia proxitok.
// @author       gonzalo 
// @match        *://*.tiktok.com/*
// @grant        none
// @run-at       document-start
// @license      MIT
// ==/UserScript==

function isProperTargetPage(url) {
    return !!url.match(/^(|http(s?):\/\/)(.*\.)?tiktok.com(\/.*|$)/gim);
}

function getNewUrl(url) {
    return 'https://proxitok.pabloferreiro.es' + url.split('tiktok.com').pop();
}

if (isProperTargetPage(window.location.href)) {
    const newUrl = getNewUrl(window.location.href)
    location.replace(newUrl);
}
